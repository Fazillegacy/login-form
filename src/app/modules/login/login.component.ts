import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;

  constructor(private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
    console.log('login component....');
    this.loginForm = this.fb.group({
      emailID: ["", []],
      password: ["", []]
    });
  }

  loginClick() {
    console.log('login click....');
    const formValue = this.loginForm.value;
    console.log('form value...', formValue);
    this.router.navigateByUrl("home");

  }

get getEmailID() {
  return this.loginForm.controls.emailID;
}

get getPassword() {
  return this.loginForm.controls.password;
}

}
